let colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let buttonList = ``;
colorList.forEach((item) => {
  let buttonColor = `<button class="color-button ${item}"></button>`;
  buttonList += buttonColor;
});
document.getElementById("colorContainer").innerHTML = buttonList;

let listColor = document.getElementById("colorContainer");
let button = listColor.querySelectorAll("button");
button[0].classList.add("active");
let house = document.getElementById("house");
for (let i = 0; i < colorList.length; i++) {
  button[i].onclick = () => {
    house.className = `house ${colorList[i]}`;
    let chosenButton = listColor.querySelector(".active");
    chosenButton.classList.remove("active");
    button[i].classList.add("active");
  };
}


