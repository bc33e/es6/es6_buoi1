let diemTb = (...diemTungMon) => {
    let tongDiem = 0;
    diemTungMon.forEach((diem) => {
        tongDiem += diem;
    });
    return tongDiem/ diemTungMon.length;
}

let btnDiemKhoi1 = () => {
    let diemToan = document.getElementById("inpToan").value*1
    let diemLy = document.getElementById("inpLy").value*1
    let diemHoa = document.getElementById("inpHoa").value*1

    document.getElementById("tbKhoi1").innerHTML = `Điểm TB: ${diemTb(diemToan, diemLy, diemHoa).toFixed(2)}`
}

let btnDiemKhoi2 = () => {
    let diemVan = document.getElementById("inpVan").value*1
    let diemSu = document.getElementById("inpSu").value*1
    let diemDia = document.getElementById("inpDia").value*1
    let diemAv = document.getElementById("inpAv").value*1

    document.getElementById("tbKhoi2").innerHTML = `Điểm TB: ${diemTb(diemVan, diemSu, diemDia,diemAv).toFixed(2)}`
}